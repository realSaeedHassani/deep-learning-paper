import glob
import os

path = '/home/saeed/Downloads/LSTM-FCN-master/deep-learning-paperdataset/'

count = 90


def remove_item_list(length):
    ri = []
    step = 30
    for i in range(0, length):
        if i % step == 0:
            ri.append(i)
    return ri


for filename in glob.iglob(path + '**/*.csv', recursive=True):
    head, tail = os.path.split(os.path.split(os.path.split(filename)[0])[0])
    with open(filename) as f:
        content = f.readlines()
        content = content[4000:-4000]
        remove_index = remove_item_list(content.__len__())
        content = [i for j, i in enumerate(content) if j in remove_index]
        print(filename)
        content[0] = 'A,' + tail + '\n'
        h, t = os.path.split(os.path.split(filename)[0])
        with open(h + t + "__" + str(count) + ".csv", 'w') as f1:
            count += 1
            for item in content:
                f1.write("%s" % item)
