import glob, os, csv

path = '/home/saeed/Downloads/LSTM-FCN-master/dataset/my_dataset/'
for filename in glob.iglob(path + '**/*.csv', recursive=True):
    with open(filename, 'r') as f:
        to_list = [line.strip().split(',')[1] for line in f]
    with open('db.csv', 'a') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_NONE)
        wr.writerow(to_list)
