path = '/home/saeed/Downloads/LSTM-FCN-master/deep-learning-paper/pre/db.csv'


def is_okay(count):
    if count % 45 == 0 \
            or count % 45 == 1 \
            or count % 45 == 2 \
            or count % 45 == 3 \
            or count % 45 == 4 \
            or count % 45 == 5 \
            or count % 45 == 6 \
            or count % 45 == 7 \
            or count % 45 == 8 \
            or count % 45 == 9:
        return True
    return False


def file_len(fname):
    with open(fname, newline='') as f:
        for i, l in enumerate(f):
            if not is_okay(i):
                with open('train_dataset.csv', 'a') as ff:
                    ff.write(l)
    return i + 1


if __name__ == '__main__':
    file_len(path)
