replacements = {'Faulty_0.1_Is_UB_1370': '1',
                'Faulty_0.1_Is_UB_1410': '2',
                'Faulty_0.1_Is_UB_1450': '3',
                'Faulty_1_Is_UB_1370': '4',
                'Faulty_1_Is_UB_1410': '5',
                'Faulty_1_Is_UB_1450': '6',
                'Faulty_3_Is_UB_1370': '7',
                'Faulty_3_Is_UB_1410': '8',
                'Faulty_3_Is_UB_1450': '9',
                'Faulty_5_Is_UB_1370': '10',
                'Faulty_5_Is_UB_1410': '11',
                'Faulty_5_Is_UB_1450': '12',
                'Faulty_7_Is_UB_1370': '13',
                'Faulty_7_Is_UB_1410': '14',
                'Faulty_7_Is_UB_1450': '15',
                'Healty_0_Is_1370': '16',
                'Healty_0_Is_1410': '17',
                'Healty_0_Is_1450': '18'
                }

input_path = '/home/saeed/Downloads/LSTM-FCN-master/deep-learning-paper/pre/train_dataset.csv'
output_path = '../data/Adiac/Adiac_TRAIN'

with open(input_path) as infile, open(output_path, 'w') as outfile:
    for line in infile:
        for src, target in replacements.items():
            line = line.replace(src, target)
        outfile.write(line)
