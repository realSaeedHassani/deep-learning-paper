import pandas as pd
df1 = pd.read_csv('alstmfcn_64_cells_weights.csv')
df2 = pd.read_csv('feature_lstm.csv')
df = pd.concat([df1 , df2],axis=1, join='inner').sort_index()
df.to_csv('final')