from pandas import read_csv

header = []

for i in range(1, 193):
    header.append("f_" + str(i))

df = read_csv('featur_alstm.csv')
df.columns = header
df.to_csv('main_alstmfcn_64_cells_weights.csv', sep=',')

df = read_csv('feature_lstm.csv')
df.columns = header
df.to_csv('main_lstmfcn_64_cells_weights.csv', sep=',')
